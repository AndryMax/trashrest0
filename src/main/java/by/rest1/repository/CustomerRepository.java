package by.rest1.repository;

import by.rest1.model.Customer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface CustomerRepository extends JpaRepository<Customer, Long> {

//    @Query("select c from Customer c where c.firstName = :firstName")
//    Customer findByName(@Param("firstName") String name);
}
