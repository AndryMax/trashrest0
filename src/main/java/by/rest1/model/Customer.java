package by.rest1.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;

import javax.persistence.Entity;
import java.math.BigDecimal;

@Entity
@Data
@NoArgsConstructor(access = AccessLevel.PRIVATE, force = true)
@EqualsAndHashCode(callSuper = true)
@RequiredArgsConstructor
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Customer extends BaseEntity{

    //@Column(name = "first_name")
    private final String firstName;

    //@Column(name = "last_name")
    private final String lastName;

    //@Column(name = "address")
    private final String address;

    //@Column(name = "budget")
    private final BigDecimal budget;
}
