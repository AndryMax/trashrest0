package by.rest1.service;

import by.rest1.model.Customer;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

public interface CustomerService {

    Optional<Customer> getById(Long id);

    Optional<Customer> getByName(String name);

    void save(Customer customer);

    void delete(Long id);

    List<Customer> getAll();
}
