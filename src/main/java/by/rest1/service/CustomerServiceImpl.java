package by.rest1.service;

import by.rest1.model.Customer;
import by.rest1.repository.CustomerRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Slf4j
@Service
public class CustomerServiceImpl implements CustomerService {

    private final CustomerRepository repository;

    @Autowired
    public CustomerServiceImpl(CustomerRepository repository) {
        this.repository = repository;
    }

    @Override
    public Optional<Customer> getById(Long id) {
        log.info("IN CustomerServiceImpl getById {}", id);
        return repository.findById(id);
    }

    @Override
    public Optional<Customer> getByName(String name) {
        log.info("IN CustomerServiceImpl getByName {}", name);
        //return Optional.ofNullable(repository.findByName(name));
        return null;
    }

    @Override
    public void save(Customer customer) {
        log.info("IN CustomerServiceImpl save {}", customer);
        repository.save(customer);
    }

    @Override
    public void delete(Long id) {
        log.info("IN CustomerServiceImpl delete {}", id);
        repository.deleteById(id);
    }

    @Override
    public List<Customer> getAll() {
        log.info("IN CustomerServiceImpl getAll {}");
        return repository.findAll();
    }
}
