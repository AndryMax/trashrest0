package by.rest1.service;

import by.rest1.model.Customer;
import by.rest1.repository.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Configuration;

import java.math.BigDecimal;

@Configuration
public class StartedData implements CommandLineRunner {

    private final CustomerRepository customerRepository;

    @Autowired
    public StartedData(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }

    @Override
    public void run(String... args) throws Exception {
        Customer customer1 = new Customer(
                "valera", "petrov", "nemiga", new BigDecimal(3000));
        Customer customer2 = new Customer(
                "ksyha", "shichko", "vostok", new BigDecimal(10000));
        Customer customer3 = new Customer(
                "petya", "ivanov", "centre", new BigDecimal(500));
        customerRepository.save(customer1);
        customerRepository.save(customer2);
        customerRepository.save(customer3);
    }
}
