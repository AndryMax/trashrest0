package by.rest1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Runner {

    private static final String CONTEXT = "applicationContext.xml";

    public static void main(String[] args) {
        SpringApplication.run(Runner.class);
    }
}
